import { Home, Login } from "./pages";

const RouteList = [
	{ path: "/", element: <Home /> },
	{ path: "/login", element: <Login /> },

	// { path: "/products", element: <ProductsPage /> },
	// { path: "/products/:category", element: <ProductsPage /> },
	// { path: "/register", element: <RegisterPage /> },
	// { path: "/product/:id", element: <ProductDetailPage /> },
	// { path: "/carts", element: <CartPage /> },
	// { path: "/success", element: <Success /> },
];

export default RouteList;
