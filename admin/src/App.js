import "./App.css";
import { Home } from "./pages";
import { Route, Routes, useLocation } from "react-router-dom";
import RouteList from "./routes";
import { useEffect } from "react";

function App() {
	const ScrollToTop = () => {
		const { pathname } = useLocation();

		useEffect(() => {
			window.scrollTo(0, 0);
		}, [pathname]);

		return null;
	};
	return (
		<div className="app">
			<ScrollToTop />
			<Routes>
				{RouteList.map((route, index) => {
					return route.path ? (
						<Route path={route.path} element={route.element} key={index} />
					) : null;
				})}
				<Route path="*" element={<Home />} />
			</Routes>
		</div>
	);
}

export default App;
