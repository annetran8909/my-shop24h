// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getAuth } from "firebase/auth";

// Your web app's Firebase configuration
const firebaseConfig = {
	apiKey: "AIzaSyCQwkgOTFxRaT-OC_BF_t_ItCpJ9e2TydM",
	authDomain: "devcamp-shop24h-7143c.firebaseapp.com",
	projectId: "devcamp-shop24h-7143c",
	storageBucket: "devcamp-shop24h-7143c.appspot.com",
	messagingSenderId: "886879007490",
	appId: "1:886879007490:web:664c416b222715260d881b",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);
export default auth;
