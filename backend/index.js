const express = require("express");
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const path = require("path");
const cors = require("cors");

const userRoute = require("./apps/routes/userRoute");
const authRoute = require("./apps/routes/authRoute");
const productRoute = require("./apps/routes/productRoute");
const cartRoute = require("./apps/routes/cartRoute");
const orderRoute = require("./apps/routes/orderRoute");
const stripeRoute = require("./apps/routes/stripeRoute");

dotenv.config();
app.use(express.json());

app.use(function (req, res, next) {
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
	res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type");
	res.setHeader("Access-Control-Allow-Credentials", true);
	next();
});

mongoose
	.connect(process.env.MONGO_URL)
	.then(() => console.log("Connect MongoDB successfully"))
	.catch((error) => console.log(error));

app.use(cors());
app.use(express.static(path.join(__dirname, "public")));

app.use("/auth", authRoute);
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/carts", cartRoute);
app.use("/orders", orderRoute);
app.use("/checkout", stripeRoute);

app.listen(8000, () => {
	console.log("Backend server is running!");
});
