const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
	{
		title: { type: String, required: true, unique: true },
		description: { type: String, required: true },
		imgUrl: { type: Array, required: true },
		categories: { type: Array },
		price: { type: Number, required: true },
		inStock: { type: Boolean, required: true },
	},
	{ timestamps: true },
);

module.exports = mongoose.model("Product", productSchema);
