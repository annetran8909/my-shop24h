const router = require("express").Router();
const User = require("../models/userModel");
const CryptoJS = require("crypto-js");
const jwt = require("jsonwebtoken");

//Register
router.post("/register", async (req, res) => {
	const newUser = new User({
		username: req.body.username,
		email: req.body.email,
		password: CryptoJS.AES.encrypt(req.body.password, process.env.PASS_HIDE).toString(),
	});

	try {
		const savedUser = await newUser.save();
		console.log(savedUser);
		res.status(201).json(savedUser);
	} catch (err) {
		res.status(500).json(err);
	}
});

//Login
router.post("/login", async (req, res) => {
	try {
		const user = await User.findOne({ username: req.body.username });
		if (!user) {
			res.status(401).json("User is not existed!");
			return;
		}
		const findPass = CryptoJS.AES.decrypt(user.password, process.env.PASS_HIDE);
		const originPassword = findPass.toString(CryptoJS.enc.Utf8);

		if (originPassword !== req.body.password) {
			res.status(401).json("Password is not correct!");
			return;
		}
		const accessToken = jwt.sign(
			{
				id: user._id,
				isAdmin: user.isAdmin,
			},
			process.env.JWT,
			{ expiresIn: "1d" },
		);
		const { password, ...others } = user._doc;
		res.status(200).json({ ...others, accessToken });
	} catch (err) {
		res.status(500).json(err);
	}
});

module.exports = router;
