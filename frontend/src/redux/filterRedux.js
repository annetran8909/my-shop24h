import { createSlice } from "@reduxjs/toolkit";

const filterSlice = createSlice({
	name: "filter",
	initialState: {
		products: [],
		currentPage: 1,
		totalPage: 0,
		size: 9,
	},
	reducers: {
		callSuccess: (state, action) => {
			state.totalPage = Math.ceil(action.data / state.size);
			state.products = action.data;
		},
		callPagination: (state, action) => {
			state.currentPage = action.page;
		},
	},
});

export const { callSuccess, callPagination } = filterSlice.actions;
export default filterSlice.reducer;
