import { createSlice } from "@reduxjs/toolkit";

const products =
	localStorage.getItem("cartList") !== null ? JSON.parse(localStorage.getItem("cartList")) : [];

const totalAmount =
	localStorage.getItem("cartTotal") !== null ? JSON.parse(localStorage.getItem("cartTotal")) : 0;

const totalQuantity =
	localStorage.getItem("cartQuantity") !== null
		? JSON.parse(localStorage.getItem("cartQuantity"))
		: 0;

const setCartListFunc = (products, totalAmount, totalQuantity) => {
	localStorage.setItem("cartList", JSON.stringify(products));
	localStorage.setItem("cartTotal", JSON.stringify(totalAmount));
	localStorage.setItem("cartQuantity", JSON.stringify(totalQuantity));
};

const cartSlice = createSlice({
	name: "cart",
	initialState: {
		products: products,
		quantity: totalQuantity,
		total: totalAmount,
	},
	reducers: {
		addProduct: (state, action) => {
			const itemInCart = state.products.find((item) => item._id === action.payload._id);
			state.quantity++;
			if (itemInCart) {
				itemInCart.quantity++;
				itemInCart.totalPrice = itemInCart.totalPrice + action.payload.price;
			} else {
				state.products.push({ ...action.payload, quantity: 1 });
			}

			state.total = state.products.reduce(
				(total, product) => total + Number(product.price) * Number(product.quantity),
				0,
			);
			setCartListFunc(
				state.products.map((item) => item),
				state.total,
				state.quantity,
			);
		},
		incrementQuantity: (state, action) => {
			const item = state.products.find((item) => item._id === action.payload);
			item.quantity++;
			setCartListFunc(
				state.products.map((item) => item),
				state.total,
				state.quantity,
			);
		},
		decrementQuantity: (state, action) => {
			const item = state.products.find((item) => item._id === action.payload);
			if (item.quantity === 1) {
				item.quantity = 1;
			} else {
				item.quantity--;
			}
			setCartListFunc(
				state.products.map((item) => item),
				state.total,
				state.quantity,
			);
		},
		removeItem: (state, action) => {
			const removeItem = state.products.filter((item) => item._id !== action.payload);
			state.products = removeItem;
			setCartListFunc(
				state.products.map((item) => item),
				state.total,
				state.quantity,
			);
		},
	},
});

export const { addProduct, incrementQuantity, decrementQuantity, removeItem } = cartSlice.actions;
export default cartSlice.reducer;
