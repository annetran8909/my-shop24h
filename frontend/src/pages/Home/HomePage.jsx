import React from "react";
import { Header, Footer } from "../../components";
import "../../components/Content/Home/Home.scss";
import {
	Categories,
	Landing,
	NewsLetter,
	ProductGallery,
	ViewAll,
} from "../../components/Content/Home";

const Homepage = () => {
	return (
		<div>
			<Header />
			<Landing />
			<Categories />
			<ProductGallery />
			<ViewAll />
			<NewsLetter />
			<Footer />
		</div>
	);
};

export default Homepage;
