import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import auth from "../../firebase";
import { GoogleAuthProvider, onAuthStateChanged, signInWithPopup } from "firebase/auth";
import "../../components/Content/Login/Login.scss";
import Login from "../../components/Content/Login/Login";

const provider = new GoogleAuthProvider();

const LoginPage = () => {
	const [user, setUser] = useState(null);
	const navigate = useNavigate();
	const loginGoogle = () => {
		signInWithPopup(auth, provider)
			.then((result) => {
				setUser(result.user);
			})
			.catch((err) => {
				console.error(err);
			});
	};
	useEffect(() => {
		onAuthStateChanged(auth, (result) => {
			console.log(result);
			setUser(result);
		});
	});
	console.log(user);
	return (
		<div className="app__login-page">
			<div className="app__login-container">
				{user ? navigate("/") : <Login loginGoogle={loginGoogle} user={user} setUser={setUser} />}
			</div>
		</div>
	);
};

export default LoginPage;
