import React, { useState } from "react";
import { Breadcrumb, Footer, Header, ProductList } from "../../components";

const ProductsPage = () => {
	const [crumb, setCrumb] = useState([
		{ name: "Homepage", path: "" },
		{ name: "All Products", path: "products" },
	]);

	return (
		<div>
			<Header />
			<Breadcrumb crumb={crumb} setCrumb={setCrumb} />
			<ProductList />
			<Footer />
		</div>
	);
};

export default ProductsPage;
