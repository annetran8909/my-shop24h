import React, { useState } from "react";
import { Breadcrumb, Footer, Header } from "../../components";
import Cart from "../../components/Content/Cart/Cart";
import { useSelector } from "react-redux";

const CartPage = () => {
	const [crumb, setCrumb] = useState([
		{ name: "Homepage", path: "" },
		{ name: "Cart", path: "cart" },
	]);

	const cart = useSelector((state) => state.cart);

	const getTotal = () => {
		let totalQuantity = 0;
		let totalPrice = 0;
		cart.products.forEach((item) => {
			totalQuantity += item.quantity;
			totalPrice += item.price * item.quantity;
		});
		return { totalPrice, totalQuantity };
	};
	console.log(cart);
	return (
		<div>
			<Header cart={cart} />
			<Breadcrumb crumb={crumb} setCrumb={setCrumb} />
			<Cart cart={cart} getTotal={getTotal} />
			<Footer />
		</div>
	);
};

export default CartPage;
