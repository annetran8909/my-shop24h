import React, { useState } from "react";
import { Breadcrumb, Footer, Header } from "../../components";
import Detail from "../../components/Content/Detail/Detail";
import { useLocation } from "react-router-dom";

const ProductDetailPage = () => {
	const { state } = useLocation();

	const [crumb, setCrumb] = useState([
		{ name: "Homepage", path: "" },
		{ name: "All Products", path: "products" },
		{ name: `${state.title}`, path: "#" },
	]);

	return (
		<div>
			<Header state={state} />
			<Breadcrumb crumb={crumb} setCrumb={setCrumb} />
			<Detail />
			<Footer />
		</div>
	);
};

export default ProductDetailPage;
