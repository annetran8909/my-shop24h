import React from "react";
import Register from "../../components/Content/Register/Register";

const RegisterPage = () => {
	return (
		<div className="app__register">
			<Register />
		</div>
	);
};

export default RegisterPage;
