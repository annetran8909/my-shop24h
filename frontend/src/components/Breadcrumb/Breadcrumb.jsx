import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./Breadcrumb.scss";

const Breadcrumb = (props) => {
	const [space, setSpace] = useState(false);
	const { crumb } = props;
	const navigate = useNavigate();
	const LastCrumb = (i) => {
		return i === crumb.length - 1;
	};
	const reload = () => window.location.reload();

	const setTopSpace = () => {
		if (window.scrollY > 30) {
			setSpace(true);
		} else {
			setSpace(false);
		}
	};
	window.addEventListener("scroll", setTopSpace);

	return (
		<div className="breadcrumb" style={{ marginTop: `${space ? "80px" : "20px"}` }}>
			{crumb.map((crumb, i) => {
				const active = LastCrumb(i) ? "active" : "";
				const pointer = LastCrumb(i) ? "none" : "auto";
				return (
					<li
						key={i}
						className={`app__breadcrumb-home breadcrumb-item ${active}`}
						style={{ pointerEvents: `${pointer}` }}>
						<h5
							onClick={() => {
								navigate(`/${crumb.path}`);
								reload();
							}}>
							{crumb.name}
						</h5>
					</li>
				);
			})}
		</div>
	);
};

export default Breadcrumb;
