import Slider from "react-slick";
import React from "react";
import "../../../assets/slick/slick.css";
import "../../../assets/slick/slick-theme.css";
import { useNavigate } from "react-router-dom";

const Landing = () => {
	const navigate = useNavigate();
	const settings = {
		dots: true,
		fade: true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
		cssEase: "linear",
	};
	return (
		<div className="app__home-landing">
			<Slider {...settings}>
				<div id="landing-slide-1">
					<div className="landing-slide-content">
						<h1>NEW OFFER</h1>
						<p>WOMEN COLLECTION</p>
						<button className="buy-now" onClick={() => navigate("/products")}>
							Buy Now
						</button>
					</div>
				</div>
				<div id="landing-slide-2">
					<div className="landing-slide-content">
						<h1>SUMMERTIME</h1>
						<p>SUMMER COLLECTION</p>
						<button className="buy-now" onClick={() => navigate("/products")}>
							Buy Now
						</button>
					</div>
				</div>
				<div id="landing-slide-3">
					<div className="landing-slide-overlay"></div>
					<div className="landing-slide-collection">
						<h1>SEASONAL COLLECTION</h1>
						<p>New Arrivals</p>
						<button className="buy-now" onClick={() => navigate("/products")}>
							Buy Now
						</button>
					</div>
				</div>
			</Slider>
		</div>
	);
};

export default Landing;
