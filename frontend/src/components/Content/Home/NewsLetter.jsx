import React from "react";
import { Send } from "@mui/icons-material";

const NewsLetter = () => {
	return (
		<div className="app__news-letter">
			<h1>Newsletter</h1>
			<p>Get timely updates from your favorite products.</p>
			<div className="app__news-letter-input">
				<input placeholder="Your email" />
				<button>
					<Send />
				</button>
			</div>
		</div>
	);
};

export default NewsLetter;
