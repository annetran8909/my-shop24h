import React from "react";
import { categories, categoriesGenre } from "../../../data/Categories";
import { useNavigate } from "react-router-dom";

const Categories = () => {
	const navigate = useNavigate();
	return (
		<div className="app__category">
			<h1 className="app__category-title">Categories</h1>
			<div className="app__category-genre">
				{categoriesGenre.map((x, i) => {
					return (
						<div key={i} className="app__category-item">
							<img src={x.img} alt={x.img} />
							<div className="app__category-info">
								<h1>{x.title}</h1>
								<button onClick={() => navigate(`/products/${x.genre}`)}>SHOP NOW</button>
							</div>
						</div>
					);
				})}
			</div>
			<div className="app__category-thing">
				{categories.map((x, i) => {
					return (
						<div key={i} className="app__category-accessory">
							<img src={x.img} alt={x.img} />
							<div className="app__category-info">
								<h1>{x.title}</h1>
								<button onClick={() => navigate(`/products/${x.category}`)}>SHOP NOW</button>
							</div>
						</div>
					);
				})}
			</div>
		</div>
	);
};

export default Categories;
