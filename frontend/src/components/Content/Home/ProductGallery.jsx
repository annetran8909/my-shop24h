import React, { useEffect } from "react";
import { FavoriteBorder, Search, ShoppingCart } from "@mui/icons-material";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import axios from "axios";

const ProductGallery = () => {
	const navigate = useNavigate();
	const [isClick, setIsClick] = useState(false);
	const [products, setProducts] = useState([]);

	useEffect(() => {
		const getProducts = async () => {
			try {
				const res = await axios.get("http://localhost:8000/products");
				setProducts(res.data);
			} catch (err) {}
		};
		getProducts();
	}, []);

	return (
		<div className="app__gallery">
			<h1 className="app__gallery-title">Gallery</h1>
			<div className="gallery-container">
				{products.slice(7, 14).map((x, i) => {
					return (
						<div className="app__gallery-container" key={i}>
							<div className="app__gallery-circle"></div>
							<img src={x.imgUrl[1].displayImg} alt={x.title} />
							<div className="app__gallery-info">
								<div className="app__gallery-icon">
									<ShoppingCart onClick={() => navigate("/carts")} />
								</div>
								<div className="app__gallery-icon">
									<Search onClick={() => navigate("/products")} />
								</div>
								<div className="app__gallery-icon">
									<FavoriteBorder
										onClick={() => setIsClick(!isClick)}
										color={isClick ? "error" : ""}
									/>
								</div>
							</div>
						</div>
					);
				})}
			</div>
		</div>
	);
};

export default ProductGallery;
