import React from "react";
import { useNavigate } from "react-router-dom";

const ViewAll = () => {
	const navigate = useNavigate();
	return (
		<div className="app__view-all">
			<button onClick={() => navigate("/products")}>View All</button>
		</div>
	);
};

export default ViewAll;
