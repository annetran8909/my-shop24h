export { default as Landing } from "./Landing";
export { default as Categories } from "./Categories";
export { default as ProductGallery } from "./ProductGallery";
export { default as ViewAll } from "./ViewAll";
export { default as NewsLetter } from "./NewsLetter";
