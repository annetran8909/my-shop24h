import React, { useState, useEffect } from "react";
import { ItemDescription, RelatedItem, SingleItem } from "./content";
import "./Detail.scss";
import { useLocation } from "react-router-dom";

const Detail = () => {
	const { state } = useLocation();
	const [quantity, setQuantity] = useState(1);

	console.log(state);

	return (
		<div className="app__detail">
			<SingleItem state={state} quantity={quantity} setQuantity={setQuantity} />
			<ItemDescription state={state} />
			<RelatedItem state={state} />
		</div>
	);
};

export default Detail;
