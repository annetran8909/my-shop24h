import React from "react";

const Content = ({ limit, more }) => {
	const text = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc efficitur, magna et porttitor efficitur, est ligula pulvinar ipsum, at laoreet magna tortor eu est. Aliquam rutrum mi eget
			diam semper placerat. Mauris id neque ornare, posuere libero nec, fermentum libero. Nullam
			placerat egestas velit, vitae imperdiet est auctor ornare. Etiam a quam at felis feugiat
			posuere at ac erat. Phasellus vel neque in nulla sagittis venenatis a non nisi. Lorem ipsum
			dolor sit amet, consectetur adipiscing elit. Pellentesque malesuada volutpat mauris et
			blandit. Nulla volutpat mi ac nunc viverra auctor. Pellentesque blandit egestas felis eget
			commodo. Nulla faucibus ante quis neque accumsan, et aliquam justo congue.`;
	return <div>{more ? text : text.substring(0, 300)}</div>;
};

export default Content;
