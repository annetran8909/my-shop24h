import React, { useState } from "react";
import Content from "./Content";

const ItemDescription = ({ state }) => {
	const [more, setMore] = useState(false);
	const [heightCurr, setHeightCurr] = useState(400);
	const [heightImg, setHeightImg] = useState(200);
	const onMoreClick = () => {
		setMore((prev) => !prev);
		setHeightCurr(more ? 400 : 800);
		setHeightImg(more ? 200 : 500);
	};

	return (
		<div className="app__detail-desc" style={{ height: `${heightCurr}px` }}>
			<h4>Description</h4>
			<p>
				<Content limit={100} more={more} />
			</p>
			<div className="img-container">
				<img src={state.imgUrl[1].displayImg} alt="" style={{ height: `${heightImg}px` }} />
			</div>
			<div className="button-container">
				<button onClick={onMoreClick}>{more ? "Less" : "More"}</button>
			</div>
		</div>
	);
};

export default ItemDescription;
