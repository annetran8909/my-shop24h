import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const RelatedItem = ({ state }) => {
	const navigate = useNavigate();
	const [products, setProducts] = useState([]);
	useEffect(() => {
		const getProducts = async () => {
			try {
				const res = await axios.get("http://localhost:8000/products");
				setProducts(res.data);
			} catch (err) {}
		};
		getProducts();
	}, []);

	let relatedList = products;
	if (state._id) {
		relatedList = relatedList.filter((item) => item.categories[0] === state.categories[0]);
	}
	const nextProduct = (i) => {
		navigate(`/product/${i._id}`, { state: i });
	};
	return (
		<div className="app__related">
			<div className="title-container">
				<h4>Related Product</h4>
			</div>
			<div className="related-container">
				<div className="inner-container animate animate">
					{relatedList.map((x, i) => {
						return (
							<div className="each-img" key={i}>
								<img src={x.imgUrl[1].displayImg} alt="" onClick={() => nextProduct(x)} />
							</div>
						);
					})}
					{relatedList.map((x, i) => {
						return (
							<div className="each-img" key={i}>
								<img src={x.imgUrl[1].displayImg} alt="" onClick={() => nextProduct(x)} />
							</div>
						);
					})}
					{relatedList.map((x, i) => {
						return (
							<div className="each-img" key={i}>
								<img src={x.imgUrl[1].displayImg} alt="" onClick={() => nextProduct(x)} />
							</div>
						);
					})}
					{relatedList.map((x, i) => {
						return (
							<div className="each-img" key={i}>
								<img src={x.imgUrl[1].displayImg} alt="" onClick={() => nextProduct(x)} />
							</div>
						);
					})}
					{relatedList.map((x, i) => {
						return (
							<div className="each-img" key={i}>
								<img src={x.imgUrl[1].displayImg} alt="" onClick={() => nextProduct(x)} />
							</div>
						);
					})}
				</div>
			</div>
		</div>
	);
};

export default RelatedItem;
