import React from "react";
import { AddCircle, RemoveCircle } from "@mui/icons-material";
import { addProduct } from "../../../../redux/cartRedux";
import { useDispatch } from "react-redux";

const SingleItem = ({ setQuantity, quantity, state }) => {
	const dispatch = useDispatch();

	const handleQuantity = (type) => {
		if (type === "dec") {
			quantity > 1 && setQuantity(quantity - 1);
		} else {
			setQuantity(quantity + 1);
		}
	};

	console.log(state);
	return (
		<div className="app__detail-single">
			<div className="app__detail-img">
				<img src={state.imgUrl[1].displayImg} alt="" />
				<div className="detail-img-row">
					<div>
						<img src={state.imgUrl[1].displayImg} alt="" />
					</div>
					<div>
						<img src={state.imgUrl[2].subImg1} alt="" />
					</div>
					<div>
						<img src={state.imgUrl[3].subImg2} alt="" />
					</div>
				</div>
			</div>
			<div className="app__item-detail">
				<h4>{state.title}</h4>
				<p>
					Type: {state.categories[0]} - For: {state.categories[1]}
				</p>
				<div className="item-detail-desc">
					<p>{state.description}</p>
				</div>
				<p>$ {state.price}</p>
				<div className="item-count">
					<div className="item-count-action">
						<RemoveCircle onClick={() => handleQuantity("dec")} />
						<p>{quantity}</p>
						<AddCircle onClick={() => handleQuantity("inc")} />
					</div>
				</div>
				<div className="item-button">
					<button onClick={() => dispatch(addProduct({ ...state, quantity }))}>ADD TO CART</button>
				</div>
			</div>
		</div>
	);
};

export default SingleItem;
