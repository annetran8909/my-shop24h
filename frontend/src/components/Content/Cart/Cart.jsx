import React from "react";
import { CartDetail, CartTotal } from "./content";
import "./Cart.scss";

const Cart = ({ cart, getTotal }) => {
	return (
		<div className="app__cart">
			<CartDetail cart={cart} />
			<CartTotal cart={cart} getTotal={getTotal} />
		</div>
	);
};

export default Cart;
