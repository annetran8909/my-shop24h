import React from "react";
import { useNavigate } from "react-router-dom";
import StripeCheckout from "react-stripe-checkout";
import { useEffect, useState } from "react";
import { userRequest } from "../../../../RequestMethod";
import { useSelector } from "react-redux";

const KEY =
	"pk_test_51MtWJyCzFa1DTpJqgqfiKoSBCe9ocEFyMiTsFHZrs4YjeY2BnlQDJFYXvJ3tYsc6CFdY2mkulg2IuNivfz9N8ElH00KAMc4fYc";

const CartTotal = ({ getTotal }) => {
	const navigate = useNavigate();
	const cart = useSelector((state) => state.cart);
	const [stripeToken, setStripeToken] = useState(null);
	const onToken = (token) => {
		setStripeToken(token);
		alert("Your order is done");
		navigate("/success", {});
	};

	useEffect(() => {
		const makeRequest = async () => {
			try {
				const res = await userRequest.post("/checkout/payment", {
					tokenId: stripeToken.id,
					amount: 500,
				});
				navigate("/success", {
					stripeData: res.data,
					products: cart,
				});
			} catch (err) {
				console.error(err);
			}
		};
		stripeToken && makeRequest();
	}, [stripeToken, cart.total, navigate]);

	return (
		<div className="app__cart-total">
			<div className="cart-total-title">
				<h3>ORDER SUMMARY</h3>
			</div>
			<div className="cart-total-price">
				<p>Total :</p>
				<p>$ {getTotal().totalPrice}</p>
			</div>
			<div className="cart-total-button">
				<StripeCheckout
					name="ANNET."
					image="https://lh3.googleusercontent.com/ogw/AOLn63G34AavMkW72FOe2YsBoDuwW7kIkUHiuhWH3cYSVg=s32-c-mo"
					billingAddress
					shippingAddress
					description={`Your total is $${cart.total}`}
					amount={cart.total * 100}
					token={onToken}
					stripeKey={KEY}>
					<button>Proceed To Checkout</button>
				</StripeCheckout>
			</div>
		</div>
	);
};

export default CartTotal;
