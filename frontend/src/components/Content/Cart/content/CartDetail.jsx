import React from "react";
import { AddCircle, DeleteForever, RemoveCircle } from "@mui/icons-material";
import { incrementQuantity, decrementQuantity, removeItem } from "../../../../redux/cartRedux";
import { useDispatch } from "react-redux";

const CartDetail = ({ cart }) => {
	const dispatch = useDispatch();
	console.log(cart);
	return (
		<div className="app__cart-detail">
			{cart.products.map((x, i) => (
				<div className="app__cart-item" key={i}>
					<div className="app__cart-img">
						<img src={x.imgUrl[1].displayImg} alt="" />
					</div>
					<div className="app__cart-info">
						<div className="app__cart-text">
							<h5>{x.title}</h5>
							<p>{x.description}</p>
							<p>$ {(x.price * x.quantity).toFixed(2)}</p>
						</div>
						<div className="app__cart-quantity">
							<div className="quantity-container">
								<RemoveCircle color="action" onClick={() => dispatch(decrementQuantity(x._id))} />
								<p>{x.quantity}</p>
								<AddCircle color="success" onClick={() => dispatch(incrementQuantity(x._id))} />
							</div>
							<div className="quantity-delete">
								<DeleteForever color="error" onClick={() => dispatch(removeItem(x._id))} />
							</div>
						</div>
					</div>
				</div>
			))}
		</div>
	);
};

export default CartDetail;
