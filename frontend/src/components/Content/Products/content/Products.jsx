import React from "react";
import { useNavigate } from "react-router-dom";
import { Card, CardBody, CardTitle, CardSubtitle } from "reactstrap";
import { Pagination, PaginationItem } from "@mui/material";
import { ArrowBack, ArrowForward } from "@mui/icons-material";

const Products = ({ resultsFound, list, products, size, page, cardPerPage, handleChangePage }) => {
	const navigate = useNavigate();
	const listCount = Math.ceil(list.length / size);
	const newList = products.slice(page * cardPerPage - cardPerPage, page * cardPerPage);
	const totalPage = Math.ceil(products.length / size);

	const onItemClick = (item) => {
		navigate(`/product/${item._id}`, { state: item });
	};

	console.log(products);

	return (
		<div className="app__products-container">
			<div className="app__products-list">
				{(resultsFound ? list : newList).map((x, i) => {
					return (
						<Card className="product-card" key={i}>
							<div
								className="product-card-img"
								style={{
									"--display-img": `url(${x.imgUrl[0].mainImg})`,
									"--hover-img": `url(${x.imgUrl[1].displayImg})`,
								}}></div>
							<CardBody className="product-card-body">
								<CardTitle className="product-card-title">{x.title}</CardTitle>
								<CardSubtitle className="product-card-subtitle">
									<p>$ {x.price}</p>
									<div>
										<button>Add to cart</button>
										<button onClick={() => onItemClick(x)}>View detail</button>
									</div>
								</CardSubtitle>
							</CardBody>
						</Card>
					);
				})}
			</div>
			<div className="app__products-pagination">
				<Pagination
					sx={{ m: "50px auto" }}
					count={resultsFound ? listCount : totalPage}
					color="secondary"
					page={page}
					onChange={handleChangePage}
					disableInitialCallback={true}
					initialPage={1}
					renderItem={(item) => (
						<PaginationItem slots={{ previous: ArrowBack, next: ArrowForward }} {...item} />
					)}
				/>
			</div>
		</div>
	);
};

export default Products;
