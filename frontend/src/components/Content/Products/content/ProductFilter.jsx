import React, { useState } from "react";
import { Search } from "@mui/icons-material";
import { categories } from "../../../../data/Categories";

const ProductFilter = ({
	setSort,
	handleFilters,
	changeType,
	selectedType,
	handleSort,
	handleSearch,
	handleEnter,
}) => {
	const [range1, setRange1] = useState("");
	const [range2, setRange2] = useState("");

	const [search, setSearch] = useState("");

	return (
		<div className="app__products-filter">
			<div className="products-filter-container">
				<h6>Name</h6>
				<div className="products-filter-input">
					<input
						value={search}
						onChange={(e) => setSearch(e.target.value)}
						onKeyDown={handleEnter}
					/>
					<Search onClick={(event) => handleSearch(search)} />
				</div>
			</div>
			<div className="products-filter-container">
				<h6>Categories</h6>
				{categories.map((x, i) => {
					return (
						<p key={i} onClick={() => changeType(x.category)} value={selectedType}>
							{x.title}
						</p>
					);
				})}
			</div>
			<div className="products-filter-container">
				<h6>Price</h6>
				<div className="products-filter-price">
					<div className="filter-min">
						<span>$</span>
						<input type={"number"} value={range1} onChange={(e) => setRange1(e.target.value)} />
					</div>
					<p className="dashed">-</p>
					<div className="filter-max">
						<span>$</span>
						<input type={"number"} value={range2} onChange={(e) => setRange2(e.target.value)} />
					</div>
					<button onClick={(event) => handleSort(range1, range2)}>Find</button>
				</div>
			</div>
			<div className="products-filter-container">
				<h6>Genre</h6>
				<select name="genre" className="products-filter-select" onChange={handleFilters}>
					<option value="unisex">Unisex</option>
					<option value="man">Man</option>
					<option value="woman">Woman</option>
				</select>
			</div>
			<div className="products-filter-container">
				<h6>Sort</h6>
				<select className="products-filter-select" onChange={(e) => setSort(e.target.value)}>
					<option value="all">All</option>
					<option value="newest">Newest</option>
					<option value="asc">Price (asc)</option>
					<option value="desc">Price (desc)</option>
				</select>
			</div>
		</div>
	);
};

export default ProductFilter;
