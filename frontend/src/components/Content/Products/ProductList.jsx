import React, { useEffect, useState } from "react";
import { ProductFilter, Products } from "./content/index";
import "./ProductList.scss";
import { useLocation, useNavigate } from "react-router-dom";
import axios from "axios";

const ProductList = () => {
	const navigate = useNavigate();
	const location = useLocation();
	const cat = location.pathname.split("/")[2];
	const [products, setProducts] = useState([]);

	useEffect(() => {
		const getProducts = async () => {
			try {
				const res = await axios.get(
					cat ? `http://localhost:8000/products?category=${cat}` : "http://localhost:8000/products",
				);
				setProducts(res.data);
			} catch (err) {}
		};
		getProducts();
	}, [cat]);
	const [list, setList] = useState(products);
	const [filters, setFilters] = useState({});
	const [sort, setSort] = useState("newest");
	const [selectedType, setSelectedType] = useState(null);
	const [selectedPrice, setSelectedPrice] = useState({ value1: "", value2: "" });
	const [searchInput, setSearchInput] = useState("");
	const [resultsFound, setResultsFound] = useState(true);
	const [page, setPage] = useState(1);
	const [cardPerPage] = useState(9);

	const handleChangePage = (event, page) => {
		setPage(page);
		navigate({
			pathname: "/products",
			search: `?page=${page}`,
		});
	};

	const handleFilters = (e) => {
		const value = e.target.value;
		setFilters({
			...filters,
			[e.target.name]: value,
		});
	};

	const handleSelectType = (item) => {
		console.log(item);
		navigate({
			pathname: "/products",
			search: `?type=${item}`,
		});
		return !item ? null : setSelectedType(item);
	};

	const handleSort = (value1, value2) => {
		setSelectedPrice({ value1: value1, value2: value2 });
		navigate({
			pathname: "/products",
			search: `?minPrice=${value1}&maxPrice=${value2}`,
		});
	};

	const handleSearch = (search) => {
		setSearchInput(search);
		navigate({
			pathname: "/products",
			search: `?name=${search}`,
		});
	};

	const handleEnter = async (e) => {
		const { value } = e.target;
		if (e.key === "Enter") {
			setSearchInput(value);
			navigate({
				pathname: "/products",
				search: `?name=${value}`,
			});
		}
	};

	const applyFilters = () => {
		let updatedList = products;
		if (selectedType) {
			updatedList = updatedList.filter((item) => item.categories[0] === selectedType);
		}
		if (selectedPrice.value1 && selectedPrice.value2) {
			const minPrice = selectedPrice.value1;
			const maxPrice = selectedPrice.value2;

			updatedList = updatedList.filter((item) => item.price >= minPrice && item.price <= maxPrice);
		}

		if (searchInput) {
			updatedList = updatedList.filter(
				(item) => item.title.toLowerCase().search(searchInput.toLowerCase().trim()) !== -1,
			);
		}

		if (sort !== "all") {
			if (sort === "newest") {
				updatedList = updatedList.sort((a, b) => a.createdAt - b.createdAt);
			} else if (sort === "asc") {
				updatedList = updatedList.sort((a, b) => a.price - b.price);
			} else if (sort === "desc") {
				updatedList = updatedList.sort((a, b) => b.price - a.price);
			}
		}

		if (filters) {
			updatedList = updatedList.filter((item) => item.categories[1] === filters.genre);
		}

		setList(updatedList);

		!updatedList.length ? setResultsFound(false) : setResultsFound(true);
	};

	useEffect(() => {
		applyFilters();
	}, [selectedType, selectedPrice, searchInput, sort, filters, products]);
	console.log(list);

	return (
		<div className="app__products">
			<ProductFilter
				changeType={handleSelectType}
				selectedType={selectedType}
				handleSort={handleSort}
				handleSearch={handleSearch}
				handleEnter={handleEnter}
				handleFilters={handleFilters}
				setSort={setSort}
			/>
			<Products
				list={list}
				resultsFound={resultsFound}
				products={products}
				handleChangePage={handleChangePage}
				page={page}
				cardPerPage={cardPerPage}
				size={9}
			/>
		</div>
	);
};

export default ProductList;
