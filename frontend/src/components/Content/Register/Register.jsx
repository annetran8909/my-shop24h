import React, { useState } from "react";
import "./Register.scss";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { register } from "../../../redux/apiCalls";

const Register = () => {
	const navigate = useNavigate();
	const [username, setUsername] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const dispatch = useDispatch();
	const { isFetching, error } = useSelector((state) => state.user);

	const handleClick = (e) => {
		e.preventDefault();
		console.log(username);
		console.log(email);
		console.log(password);
		register(dispatch, { username, email, password });
	};
	return (
		<div className="app__register-container">
			<h1>CREATE AN ACCOUNT</h1>
			<div className="app__register-form">
				<input placeholder="Full name *" />
				<input placeholder="Phone number *" />
				<input placeholder="Username *" onChange={(e) => setUsername(e.target.value)} />
				<input type={"email"} placeholder="Email *" onChange={(e) => setEmail(e.target.value)} />
				<input
					type={"password"}
					placeholder="Password *"
					onChange={(e) => setPassword(e.target.value)}
				/>
				<input type={"password"} placeholder="Confirm password *" />
				<span>
					By creating an account, I consent to the processing of my personal data in accordance with
					the <b>PRIVACY POLICY</b>
				</span>
				<div>
					<button onClick={handleClick} disabled={isFetching}>
						CREATE
					</button>
				</div>
				<div className="app__register-question">
					<p>
						Have an account?
						<span onClick={() => navigate("/login")}>Sign in</span>
					</p>
				</div>
			</div>
		</div>
	);
};

export default Register;
