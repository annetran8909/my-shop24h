import React, { useState } from "react";
import GoogleIcon from "@mui/icons-material/Google";
import FacebookIcon from "@mui/icons-material/Facebook";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../../../redux/apiCalls";

const Login = ({ loginGoogle, user, setUser }) => {
	const navigate = useNavigate();
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const dispatch = useDispatch();
	const { isFetching, error } = useSelector((state) => state.user);

	const handleClick = (e) => {
		e.preventDefault();
		console.log(username);
		console.log(password);
		login(dispatch, setUser({ username, password }));
	};
	return (
		<>
			<div className="app__login-signin">
				<button onClick={loginGoogle}>
					<GoogleIcon sx={{ mr: 1 }} />
					Sign in with Google
				</button>
				<button>
					<FacebookIcon sx={{ mr: 1 }} />
					Sign in with Facebook
				</button>
			</div>
			<div className="app__login-divider">
				<div></div>
				<div>or</div>
			</div>
			<div className="app__login-form">
				<input
					className="form-control"
					placeholder="Username"
					onChange={(e) => setUsername(e.target.value)}
				/>
				<input
					className="form-control"
					placeholder="Password"
					type="password"
					onChange={(e) => setPassword(e.target.value)}
				/>
				<button onClick={handleClick} disabled={isFetching}>
					Sign in
				</button>
				{error && <span className="error-text">Something went wrong ...</span>}
			</div>
			<div className="app__login-question">
				<p>
					Don't have an account?
					<span onClick={() => navigate("/register")}>Sign up</span>
				</p>
			</div>
		</>
	);
};

export default Login;
