import React from "react";
import { useNavigate } from "react-router-dom";

const Brand = () => {
	const navigate = useNavigate();
	return (
		<div className="app__header-logo">
			<h1 onClick={() => navigate("/")}>ANNET.</h1>
		</div>
	);
};

export default Brand;
