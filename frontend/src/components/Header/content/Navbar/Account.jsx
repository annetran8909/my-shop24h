import React, { useEffect, useState } from "react";
import { Logout, Settings, AccountCircle } from "@mui/icons-material";
import { Divider, IconButton, ListItemIcon, Menu, MenuItem, Tooltip } from "@mui/material";
import { onAuthStateChanged, signOut } from "firebase/auth";
import { useNavigate } from "react-router-dom";
import auth from "../../../../firebase";
import { useSelector } from "react-redux";

const Account = () => {
	const navigate = useNavigate();
	const [user, setUser] = useState(auth.currentUser);
	const [anchorEl, setAnchorEl] = React.useState(null);
	const open = Boolean(anchorEl);
	const userLog = useSelector((state) => state.user.currentUser);
	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};
	const handleClose = () => {
		setAnchorEl(null);
	};
	const logoutGoogle = () => {
		signOut(auth)
			.then((result) => {
				console.log(result);
				setUser(null);
			})
			.catch((err) => {
				console.error(err);
			});
	};
	useEffect(() => {
		onAuthStateChanged(auth, (result) => {
			console.log(result);
			setUser(result);
		});
	});
	console.log(userLog);
	return (
		<>
			{user && user !== null ? (
				<>
					<Tooltip title={user.displayName}>
						<IconButton
							onClick={handleClick}
							aria-controls={open ? "account-menu" : undefined}
							aria-haspopup="true"
							aria-expanded={open ? "true" : undefined}>
							<img src={user.photoURL} alt="" className="login-image" />
						</IconButton>
					</Tooltip>
					<Menu
						anchorEl={anchorEl}
						id="account-menu"
						open={open}
						onClose={handleClose}
						onClick={handleClose}
						PaperProps={{
							elevation: 0,
							sx: {
								overflow: "visible",
								filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
								mt: 1.5,
								"& .MuiAvatar-root": {
									width: 32,
									height: 32,
									ml: -0.5,
									mr: 1,
								},
								"&:before": {
									content: '""',
									display: "block",
									position: "absolute",
									top: 0,
									right: 14,
									width: 10,
									height: 10,
									bgcolor: "background.paper",
									transform: "translateY(-50%) rotate(45deg)",
									zIndex: 0,
								},
							},
						}}
						transformOrigin={{ horizontal: "right", vertical: "top" }}
						anchorOrigin={{ horizontal: "right", vertical: "bottom" }}>
						<MenuItem>{user.displayName}</MenuItem>
						<MenuItem>Profile</MenuItem>
						<Divider />
						<MenuItem>
							<ListItemIcon>
								<Settings fontSize="small" />
							</ListItemIcon>
							Settings
						</MenuItem>
						<MenuItem onClick={logoutGoogle}>
							<ListItemIcon>
								<Logout fontSize="small" />
							</ListItemIcon>
							Logout
						</MenuItem>
					</Menu>
				</>
			) : userLog && userLog !== null ? (
				<>
					<Tooltip title={userLog.username}>
						<IconButton
							onClick={handleClick}
							aria-controls={open ? "account-menu" : undefined}
							aria-haspopup="true"
							aria-expanded={open ? "true" : undefined}>
							<AccountCircle />
						</IconButton>
					</Tooltip>
					<Menu
						anchorEl={anchorEl}
						id="account-menu"
						open={open}
						onClose={handleClose}
						onClick={handleClose}
						PaperProps={{
							elevation: 0,
							sx: {
								overflow: "visible",
								filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
								mt: 1.5,
								"& .MuiAvatar-root": {
									width: 32,
									height: 32,
									ml: -0.5,
									mr: 1,
								},
								"&:before": {
									content: '""',
									display: "block",
									position: "absolute",
									top: 0,
									right: 14,
									width: 10,
									height: 10,
									bgcolor: "background.paper",
									transform: "translateY(-50%) rotate(45deg)",
									zIndex: 0,
								},
							},
						}}
						transformOrigin={{ horizontal: "right", vertical: "top" }}
						anchorOrigin={{ horizontal: "right", vertical: "bottom" }}>
						<MenuItem>{userLog.username}</MenuItem>
						<MenuItem>Profile</MenuItem>
						<Divider />
						<MenuItem>
							<ListItemIcon>
								<Settings fontSize="small" />
							</ListItemIcon>
							Settings
						</MenuItem>
						<MenuItem onClick={logoutGoogle}>
							<ListItemIcon>
								<Logout fontSize="small" />
							</ListItemIcon>
							Logout
						</MenuItem>
					</Menu>
				</>
			) : (
				<>
					<IconButton baseClassName={"material-icons-outlined"}>
						<AccountCircle />
					</IconButton>
					<ul>
						<li>
							<p className="login" onClick={() => navigate("/login")}>
								Login
							</p>
						</li>
						<span>/</span>
						<li>
							<p className="register" onClick={() => navigate("/register")}>
								Register
							</p>
						</li>
					</ul>
				</>
			)}
		</>
	);
};

export default Account;
