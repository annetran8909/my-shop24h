import React, { useState } from "react";
import { Brand, NavIcons } from "./index";
import "./Navbar.scss";
import { useSelector } from "react-redux";

const Navbar = () => {
	const [fix, setFix] = useState(false);
	const setFixed = () => {
		if (window.scrollY > 30) {
			setFix(true);
		} else {
			setFix(false);
		}
	};
	window.addEventListener("scroll", setFixed);

	const product = useSelector((redux) => redux.cart.products);
	const getTotalQuantity = () => {
		let total = 0;
		product.forEach((item) => {
			total += item.quantity;
		});
		return total;
	};

	return (
		<div className={fix ? "app__header-navbar-fixed" : "app__header-navbar"}>
			<Brand />
			<NavIcons getTotalQuantity={getTotalQuantity} />
		</div>
	);
};

export default Navbar;
