import React from "react";
import { Badge } from "@mui/material";
import { NotificationsNone, ShoppingCart } from "@mui/icons-material";
import { Account } from "./index";
import { useNavigate } from "react-router-dom";

const NavIcons = ({ getTotalQuantity }) => {
	const navigate = useNavigate();
	return (
		<div className="app__header-icons">
			<div>
				<Badge badgeContent={4} color="primary" variant="dot">
					<NotificationsNone />
				</Badge>
			</div>
			<div>
				<Badge badgeContent={getTotalQuantity() || 0} color="error">
					<ShoppingCart onClick={() => navigate("/carts")} />
				</Badge>
			</div>
			<div>
				<Account />
			</div>
		</div>
	);
};

export default NavIcons;
