import React from "react";
import "./Announcement.scss";

const Announcement = () => {
	return <div className="app__header-announcement">Free shipping for standard order over $100</div>;
};

export default Announcement;
