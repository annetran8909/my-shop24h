import React from "react";
import { Navbar, Announcement } from "./content";
import "./Header.scss";

const Header = () => {
	return (
		<div>
			<Announcement />
			<Navbar />
		</div>
	);
};

export default Header;
