import React from "react";
import { Left, Center, Right } from "./Content";
import "./Footer.scss";

const Footer = () => {
	return (
		<div className="app__footer">
			<Left />
			<Center />
			<Right />
		</div>
	);
};

export default Footer;
