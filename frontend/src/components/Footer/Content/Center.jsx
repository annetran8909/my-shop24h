import React from "react";

const links = [
	"HomePage",
	"Cart",
	"For Man",
	"For Woman",
	"My Account",
	"Order Tracking",
	"Terms",
	"Notification",
];

const Center = () => {
	return (
		<div className="app__footer-center">
			<h3>Useful Links</h3>
			<ul>
				{links.map((item, index) => {
					return <li key={index}>{item}</li>;
				})}
			</ul>
		</div>
	);
};

export default Center;
