import React from "react";
import { Facebook, Instagram, Pinterest, Twitter } from "@mui/icons-material";
import { useNavigate } from "react-router-dom";

const Left = () => {
	const navigate = useNavigate();
	return (
		<div className="app__footer-left">
			<h1 onClick={() => navigate("/")}>ANNET.</h1>
			<p>Get the perfect accessory for any occasion!</p>
			<div className="footer-left-icons">
				<div>
					<Facebook />
				</div>
				<div>
					<Instagram />
				</div>
				<div>
					<Twitter />
				</div>
				<div>
					<Pinterest />
				</div>
			</div>
		</div>
	);
};

export default Left;
