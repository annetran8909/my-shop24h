import React from "react";
import { MailOutline, Phone, Room } from "@mui/icons-material";
import { Amazon, AmericanExpress, Discover, MasterCard, PayPal, Visa } from "../../../assets";

const Right = () => {
	return (
		<div className="app__footer-right">
			<h3>Contact</h3>
			<div className="footer-right-content">
				<p>
					<Room /> 677 Drive Hill , South Tobinchester 98336
				</p>
				<p>
					<Phone /> +1 234 56 789
				</p>
				<p>
					<MailOutline /> contact@annet.com
				</p>
			</div>
			<div className="footer-right-cards">
				<img src={MasterCard} alt="MasterCard" />
				<img src={Visa} alt="Visa" />
				<img src={AmericanExpress} alt="AmericanExpress" />
				<img src={Discover} alt="Discover" />
				<img src={PayPal} alt="PayPal" />
				<img src={Amazon} alt="Amazon" />
			</div>
		</div>
	);
};

export default Right;
