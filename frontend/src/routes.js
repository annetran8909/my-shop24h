import {
	HomePage,
	ProductsPage,
	ProductDetailPage,
	CartPage,
	LoginPage,
	RegisterPage,
	Success,
} from "./pages";

const RouteList = [
	{ path: "/", element: <HomePage /> },
	{ path: "/products", element: <ProductsPage /> },
	{ path: "/products/:category", element: <ProductsPage /> },
	{ path: "/login", element: <LoginPage /> },
	{ path: "/register", element: <RegisterPage /> },
	{ path: "/product/:id", element: <ProductDetailPage /> },
	{ path: "/carts", element: <CartPage /> },
	{ path: "/success", element: <Success /> },
];

export default RouteList;
