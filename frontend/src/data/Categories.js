export const categories = [
	{
		id: 1,
		img: "https://images.pexels.com/photos/2905238/pexels-photo-2905238.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
		title: "Bags",
		category: "bags",
	},
	{
		id: 2,
		img: "https://images.pexels.com/photos/2529148/pexels-photo-2529148.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
		title: "Footwear",
		category: "footwear",
	},
	{
		id: 3,
		img: "https://images.pexels.com/photos/177332/pexels-photo-177332.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
		title: "Jewelry",
		category: "jewelry",
	},
	{
		id: 4,
		img: "https://images.pexels.com/photos/984619/pexels-photo-984619.jpeg?auto=compress&cs=tinysrgb&w=600",
		title: "Hats",
		category: "hats",
	},
	{
		id: 5,
		img: "https://images.pexels.com/photos/12471735/pexels-photo-12471735.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
		title: "Hair Accessories",
		category: "hair accessories",
	},
	{
		id: 6,
		img: "https://images.pexels.com/photos/15116862/pexels-photo-15116862.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
		title: "Belts",
		category: "belts",
	},
	{
		id: 7,
		img: "https://images.pexels.com/photos/6344235/pexels-photo-6344235.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
		title: "Eyewear",
		category: "eyewear",
	},
	{
		id: 8,
		img: "https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/451588002/item/goods_38_451588002.jpg?width=750",
		title: "Scarves",
		category: "scarves",
	},
];

export const categoriesGenre = [
	{
		id: 1,
		img: "https://images.pexels.com/photos/1926769/pexels-photo-1926769.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
		title: "WOMEN",
		genre: "woman",
	},
	{
		id: 2,
		img: "https://images.pexels.com/photos/840916/pexels-photo-840916.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
		title: "MEN",
		genre: "man",
	},
];
